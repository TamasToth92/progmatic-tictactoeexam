/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tomi
 */
public class Board implements com.progmatic.tictactoeexam.interfaces.Board {
    ArrayList<Cell> cellsOnBoard;
    PlayerType[][] gameField;

    public Board() {
        this.gameField = new PlayerType[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                gameField[i][j] = PlayerType.EMPTY;
            }
        }
    }

    @Override
    public PlayerType getCell(int rowIdx, int colIdx) throws CellException {
        if (rowIdx > 2 || rowIdx < 0) {
            throw new CellException(rowIdx, colIdx, "The given row is not on the board!");
        } else if (colIdx > 2 || colIdx < 0) {
            throw new CellException(rowIdx, colIdx, "The given column is not on the board!");
        } else {
            return gameField[colIdx][rowIdx];
        }
    }

    @Override
    public void put(Cell cell) throws CellException {
        int colIdx = cell.getCol();
        int rowIdx = cell.getRow();
        if (rowIdx > 2 || rowIdx < 0) {
            throw new CellException(rowIdx, colIdx, "The given row is not on the board!");
        } else if (colIdx > 2 || colIdx < 0) {
            throw new CellException(rowIdx, colIdx, "The given column is not on the board!");
        } else if (gameField[colIdx][rowIdx] != PlayerType.EMPTY) {
            throw new CellException(rowIdx, colIdx, "The cell contains an element.");
        } else {
            gameField[colIdx][rowIdx] = cell.getCellsPlayer();
        }
    }

    @Override
    public List<Cell> emptyCells() {
        List<Cell> emptyCellList = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (gameField[i][j].equals(PlayerType.EMPTY)) {
                    Cell c = new Cell(i, j);
                    emptyCellList.add(c);
                }
            }
        }
        return emptyCellList;
    }

    //    String[][] test = {
    //        {"X", "O", " "},
    //        {"O", "O", "X"},
    //        {"O", "X", "O"}};
    @Override
    public boolean hasWon(PlayerType p) {
        boolean winner = false;
        if (p == gameField[0][0] && gameField[1][1] == p && p == gameField[2][2]) {
            winner = true;
            return winner;
        }
        if (p == gameField[1][1] && p == gameField[0][2] && p == gameField[2][0]) {
            winner = true;
            return winner;
        }
        for (int i = 0; i < 3; i++) {
            if (p == gameField[i][0] && p == gameField[i][2] && p == gameField[i][1]) {
                winner = true;
                return winner;
            }
        }
        for (int i = 0; i < 3; i++) {
            if (p == gameField[0][i] && p == gameField[1][i] && p == gameField[2][i]) {
                winner = true;
                return winner;
            }
        }
        return winner;
    }

}
