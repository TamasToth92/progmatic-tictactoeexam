/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.AbstractPlayer;
import com.progmatic.tictactoeexam.interfaces.Board;

/**
 *
 * @author Tomi
 */
public class SmartPlayer extends AbstractPlayer {

    public SmartPlayer(PlayerType p) {
        super(p);
    }

    @Override
    public Cell nextMove(Board b) {
        Cell c = null;
        PlayerType mine = this.getMyType();
        try {
            if (b.getCell(0, 0).equals(mine)
                    && (b.getCell(0, 1).equals(mine)
                    && b.getCell(0, 2).equals(PlayerType.EMPTY))) {
                c = new Cell(0, 2, this.myType);
                return c;
            } else if (b.getCell(0, 0).equals(mine)
                    && b.getCell(0, 1).equals(PlayerType.EMPTY)
                    && b.getCell(0, 2).equals(mine)) {
                c = new Cell(0, 1, this.myType);
                return c;
            } else if (b.getCell(0, 0).equals(PlayerType.EMPTY)
                    && b.getCell(0, 1).equals(mine)
                    && b.getCell(0, 2).equals(mine)) {
                c = new Cell(0, 0, this.myType);
                return c;

            } else if (b.getCell(0, 0).equals(mine)
                    && (b.getCell(1, 0).equals(mine)
                    && b.getCell(2, 0).equals(PlayerType.EMPTY))) {
                c = new Cell(2, 0, this.myType);
                return c;
            } else if (b.getCell(0, 0).equals(mine)
                    && b.getCell(1, 0).equals(PlayerType.EMPTY)
                    && b.getCell(2, 0).equals(mine)) {
                c = new Cell(1, 0, this.myType);
                return c;
            } else if (b.getCell(0, 0).equals(PlayerType.EMPTY)
                    && b.getCell(1, 2).equals(mine)
                    && b.getCell(2, 0).equals(mine)) {
                c = new Cell(0, 0, this.myType);
                return c;

            } else if (b.getCell(0, 0).equals(mine)
                    && (b.getCell(1, 1).equals(mine)
                    && b.getCell(2, 2).equals(PlayerType.EMPTY))) {
                c = new Cell(2, 2, this.myType);
                return c;
            } else if (b.getCell(0, 0).equals(mine)
                    && b.getCell(1, 1).equals(PlayerType.EMPTY)
                    && b.getCell(2, 2).equals(mine)) {
                c = new Cell(1, 1, this.myType);
                return c;
            } else if (b.getCell(0, 0).equals(PlayerType.EMPTY)
                    && b.getCell(1, 1).equals(mine)
                    && b.getCell(2, 2).equals(mine)) {
                c = new Cell(0, 0, this.myType);
                return c;

            } else if (b.getCell(2, 0).equals(mine)
                    && (b.getCell(1, 1).equals(mine)
                    && b.getCell(0, 2).equals(PlayerType.EMPTY))) {
                c = new Cell(0, 2, this.myType);
                return c;
            } else if (b.getCell(2, 0).equals(mine)
                    && b.getCell(1, 1).equals(PlayerType.EMPTY)
                    && b.getCell(0, 2).equals(mine)) {
                c = new Cell(1, 1, this.myType);
                return c;
            } else if (b.getCell(2, 0).equals(PlayerType.EMPTY)
                    && b.getCell(1, 1).equals(mine)
                    && b.getCell(0, 2).equals(mine)) {
                c = new Cell(2, 0, this.myType);
                return c;

            } else if (b.getCell(2, 0).equals(mine)
                    && (b.getCell(1, 2).equals(mine)
                    && b.getCell(2, 2).equals(PlayerType.EMPTY))) {
                c = new Cell(2, 2, this.myType);
                return c;
            } else if (b.getCell(2, 0).equals(mine)
                    && b.getCell(1, 2).equals(PlayerType.EMPTY)
                    && b.getCell(2, 2).equals(mine)) {
                c = new Cell(1, 2, this.myType);
                return c;
            } else if (b.getCell(2, 0).equals(PlayerType.EMPTY)
                    && b.getCell(1, 2).equals(mine)
                    && b.getCell(2, 2).equals(mine)) {
                c = new Cell(2, 0, this.myType);
                return c;
                
                
            }else if (b.getCell(0, 2).equals(mine)
                    && (b.getCell(2, 1).equals(mine)
                    && b.getCell(2, 2).equals(PlayerType.EMPTY))) {
                c = new Cell(2, 2, this.myType);
                return c;
            } else if (b.getCell(0, 2).equals(mine)
                    && b.getCell(2, 1).equals(PlayerType.EMPTY)
                    && b.getCell(2, 2).equals(mine)) {
                c = new Cell(2, 1, this.myType);
                return c;
            } else if (b.getCell(2, 0).equals(PlayerType.EMPTY)
                    && b.getCell(2, 1).equals(mine)
                    && b.getCell(2, 2).equals(mine)) {
                c = new Cell(0, 2, this.myType);
                return c;
            }

        } catch (CellException exc) {
            System.out.println("Invalid move!");
        }
        return c;
    }
}
