/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.AbstractPlayer;
import com.progmatic.tictactoeexam.interfaces.Board;

/**
 *
 * @author Tomi
 */
public class SimplePlayer extends AbstractPlayer {

    public SimplePlayer(PlayerType p) {
        super(p);
    }

    @Override
    public Cell nextMove(Board gamefield) {
        Cell c = null;
        try {
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    if (gamefield.getCell(i, j) != PlayerType.X && gamefield.getCell(i, j) != PlayerType.O) {
                        c = new Cell(i, j, this.myType);
                        return c;
                    }
                }
            }
        } catch (CellException exc) {
            System.out.println("Invalid move!");
        }
        return c;
    }

}
